//
//  ValidationViewModel.swift
//  loginTask
//
//  Created by Creative Company on 5/22/19.
//  Copyright © 2019 Creative Company. All rights reserved.
//

import UIKit
import RxSwift

protocol ValidationViewModel {
    
    var errorMessage: String { get }
    
    // Observables
    var data: Variable<String> { get set }
    var errorValue: Variable<String?> { get}
    
    // Validation
    func validateCredentials() -> Bool
}
