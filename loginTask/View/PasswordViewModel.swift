//
//  PasswordViewModel.swift
//  loginTask
//
//  Created by Creative Company on 5/22/19.
//  Copyright © 2019 Creative Company. All rights reserved.
//

import Foundation
import RxSwift


class PasswordViewModel : ValidationViewModel {
    
    var errorMessage: String = "Please enter a valid Password"
    
    var data: Variable<String> = Variable("")
    var errorValue: Variable<String?> = Variable("")
    
    func validateCredentials() -> Bool {
        
        guard validateLength(text: data.value, size: (6,15)) else{
            errorValue.value = errorMessage
            return false;
        }
        
        errorValue.value = ""
        return true
    }
    
    func validateLength(text : String, size : (min : Int, max : Int)) -> Bool{
        let passwordRegEx = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}$"
        
        let passordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passordTest.evaluate(with: text)
    }
}
