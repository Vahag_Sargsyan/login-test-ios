//
//  LoginViewModel.swift
//  loginTask
//
//  Created by Creative Company on 5/22/19.
//  Copyright © 2019 Creative Company. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire


class LoginViewModel {
    
    let model : LoginModel = LoginModel()
    let disposebag = DisposeBag()
    
    // Initialise ViewModel's
    let emailIdViewModel = EmailIdViewModel()
    let passwordViewModel = PasswordViewModel()
    
    // Fields that bind to our view's
    let isSuccess : Variable<Bool> = Variable(false)
    let isLoading : Variable<Bool> = Variable(false)
    let errorMsg : Variable<String> = Variable("")
    
    func validateCredentials() -> Bool{
        return emailIdViewModel.validateCredentials() && passwordViewModel.validateCredentials();
    }
    
    func loginUser(_ success: @escaping (_ object: Any) -> Void, failure: @escaping (_ bject: Any) -> Void){
        
        // Initialise model with filed values
        model.email = emailIdViewModel.data.value
        model.password = passwordViewModel.data.value
        
        self.isLoading.value = true
        
    
        let urlString = "http://morelly-json.cloud-guru.ml/api/test-login"
        let parameters: [String: Any] = [
                        "email": model.email,
                        "password": model.password,
                    ]
        
        Alamofire.request(urlString, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                print(response)
                self.isLoading.value = false
                self.isSuccess.value = true
                success(response as Any)
                
                break
            case .failure(let error):
                self.isLoading.value = false
                self.errorMsg.value = "error.message"
                failure(error as Any)
                
                print(error)
            }
        }
     
    }
    
}
