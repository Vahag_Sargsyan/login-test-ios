//
//  LoginModel.swift
//  loginTask
//
//  Created by Creative Company on 5/22/19.
//  Copyright © 2019 Creative Company. All rights reserved.
//

import Foundation

class LoginModel {
    
    var email = ""
    var password = ""
    
    convenience init(email : String, password : String) {
        self.init()
        self.email = email
        self.password = password
    }
}
